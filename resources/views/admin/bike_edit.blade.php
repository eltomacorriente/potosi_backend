@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edición de Bicicleta') }}</div>



                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


                    <form method="POST" action="{{ route('bike_update' , ['bike' => $bike]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Bicicleta') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{  $bike->name}}"
                                       required autocomplete="name" autofocus disabled>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="modelSelect" class="col-md-4 col-form-label text-md-right">
                                {{ __('Modelo') }}
                            </label>
                            <div class="col-md-6 form-row align-items-center pr-1">
                                <div class="col-md-6">
                                    <select 
                                        class="custom-select @error('model') is-invalid @enderror" 
                                        id="modelSelect" 
                                        name="model"
                                        required
                                        >
                                        <option></option>
                                        @foreach (['Adulto' => 'Adulto' , 'Niño' => 'Niño'] as  $model => $modelName)

                                        <option value="{{$model}}"
                                                @if($bike->model == $model)
                                                selected                                                
                                                @endif

                                                >{{$modelName}}</option> 
                                        @endforeach
                                    </select>

                                    @error('model')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                             

                            </div>
                        </div>
                        
                        
                        
                        
                         <div class="form-group row">
                            <label for="statusSelect" class="col-md-4 col-form-label text-md-right">
                                {{ __('Estado') }}
                            </label>
                            <div class="col-md-6 form-row align-items-center pr-1">
                                <div class="col-md-6">
                                    <select 
                                        class="custom-select @error('status') is-invalid @enderror" 
                                        id="statusSelect" 
                                        name="status"
                                        required
                                        >
                                        <option></option>
                                        @foreach (['enabled' => 'Habilitada' , 'disabled' => 'Deshabilitada'] as  $status => $statusName)

                                        <option value="{{$status}}"
                                                @if($bike->status == $status)
                                                selected                                                
                                                @endif

                                                >{{$statusName}}</option> 
                                        @endforeach
                                    </select>

                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                             

                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group row">
                            <label for="lockSelect" class="col-md-4 col-form-label text-md-right">
                                {{ __('Candado') }}
                            </label>
                            <div class="col-md-6 form-row align-items-center pr-1">
                                <div class="col-md-6">
                                    <select 
                                        class="custom-select @error('lock') is-invalid @enderror" 
                                        id="lockSelect" 
                                        name="lock"
                                        required
                                        >
                                        <option></option>
                                        @foreach ($locks as $lock)

                                        <option value="{{$lock->id}}"
                                                @if($bike->lock_id == $lock->id)
                                                selected                                                
                                                @endif

                                                >{{$lock->qr_code}}</option> 
                                        @endforeach
                                    </select>

                                    @error('lock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                             

                            </div>
                        </div>
                        
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Actualizar') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
