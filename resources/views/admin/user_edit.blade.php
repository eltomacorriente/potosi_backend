@extends('layouts.app')

@section('content')
<div class="container mt-5 pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edición de Usuario') }}</div>



                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


                    <form method="POST" action="{{ route('user_update' , ['user' => $user]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{  $user->name }}"
                                       required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text"
                                       class="form-control @error('last_name') is-invalid @enderror"
                                       name="last_name" value="{{ $user->last_name }}"
                                       required autocomplete="last_name" autofocus>

                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="identification" class="col-md-4 col-form-label text-md-right">{{ __('Número de identificación') }}</label>
                            <div class="col-md-6 form-row align-items-center pr-1">
                                <div class="col-md-3">
                                    <select 
                                        class="custom-select " 
                                        id="exampleFormControlSelect1"
                                        name="identification_type"
                                        required>
                                        <option></option>
                                        @foreach (["CC" , "TI" , "CE"]  as $identification_type)
                                        <option 
                                            @if($user->identification_type == $identification_type)
                                            selected                                                
                                            @endif

                                            >{{ $identification_type }}</option> 
                                        @endforeach

                                    </select>
                                    @error('identification_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-md-9">
                                    <input id="identification" 
                                           type="number"
                                           min="1000" 
                                           class="form-control @error('identification') is-invalid @enderror"
                                           name="identification" value="{{ $user->identification }}"
                                           required autocomplete="identification" autofocus>

                                    @error('identification')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categorySelect" class="col-md-4 col-form-label text-md-right">
                                {{ __('Tipo de usuario') }}
                            </label>
                            <div class="col-md-6">

                                <select 
                                    class="custom-select @error('category') is-invalid @enderror" 
                                    id="categorySelect" 
                                    name="category"
                                    required

                                    >
                                    <option></option>
                                    @foreach ($roles as $role)
                                    <option value="{{$role->id}}"
                                            @if ($role->id == $user->role->id)
                                            selected
                                            @endif
                                            >
                                            {{$role->name}}
                                </option> 


                                @endforeach
                            </select>

                            @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror


                        </div>
                    </div>

                    <div class="form-group row">
                        <label id="label_category_information" for="category_information" class="col-md-4 col-form-label text-md-right">
                            {{ __('Información Adicional') }}
                        </label>

                        <div class="col-md-6">
                            <input id="category_information" 
                                   class="form-control @error('category_information') is-invalid @enderror"
                                   name="category_information" value="{{ $user->category_information }}"
                                   autocomplete="category_information" autofocus  >

                            @error('category_information')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">
                            {{ __('strings_register.phone_number') }}
                        </label>

                        <div class="col-md-6">
                            <input id="phone_number" 
                                   type="number"
                                   min="1000000" max="9999999999"
                                   class="form-control @error('phone_number') is-invalid @enderror"
                                   name="phone_number" value="{{ $user->phone_number }}"
                                   required autocomplete="phone_number" autofocus>

                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>


                    <div class="form-group row">
                        <label for="gender" class="col-md-4 col-form-label text-md-right">
                            {{ __("Género") }}
                        </label>


                        <div class="col-md-6">
                            <select 
                                class="custom-select  @error('gender') is-invalid @enderror"
                                id="gender_select" 
                                name="gender"
                                required>

                                <option></option>

                                @foreach (["Masculino" , "Femenino" , "Otro"]  as $gender)
                                <option 
                                    @if($user->gender == $gender)
                                    selected                                                
                                    @endif

                                    >{{ $gender }}</option> 
                                @endforeach


                            </select>
                        </div>
                        @error('gender')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>



                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>



                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Actualizar') }}
                            </button>
                        </div>                       

                    </div>


                </form>

                <div class="col-md-6 mx-auto pt-3">
                     <form method="POST" action="{{ route('user_reset_password' , ['user' => $user]) }}">
                        @csrf
                        <button type="submit" class="btn btn-danger">
                            {{ __('Reiniciar contraseña') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="vue-table">
    <ride-by-user   user_id={{$user->id}}>

    </ride-by-user>

</div >

@endsection
