@extends('layouts.app' )

@section('content')
<div class="container mt-5 mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header h1">{{__('message.ayuda_title')}}</div>

                <div class="card-body">
                    <h2>
                        Este es el programa de La pradera en bicicleta si presenta problemas técnicos
                        comuníquese al celular <strong> 3125233780 </strong>.
                    </h2>
                    <br>
                    <h3>
                        Preguntas frecuentes:
                    </h3>
                    <h5>
                        ¿Dónde puedo encontrar bicicletas?
                    </h5>
                    <p> 
                        Se encuentran distribuidas por todo el club en 13 estaciones habilitadas que se
                        pueden identificar en el mapa.
                    </p>
                    <h5>
                        ¿Cómo saber el número de la bicicleta que voy a desbloquear?
                    </h5>
                    <p>
                        El número aparece al oprimir el icono de la bicicleta que desee desbloquear.

                    <h5>
                        ¿Como se que el candado se ha desactivado?
                    </h5>
                    <p>                        

                        Cuando el candado se esté desactivando, hará un sonido que indicara que está en
                        proceso de desactivación, la barra que lo asegura se sube y la bicicleta puede rodar.
                        En el mapa el color del icono de la bicicleta cambiará e indicará que se está en uso.
                    </p>
                    <h5>
                        ¿Cómo finalizo mi recorrido?
                    </h5>
                    <p>
                        Para devolver la bicicleta debe bloquear el candado manualmente y automáticamente
                        dejara de aparecer en uso con su usuario.
                    </p>
                    <h5>
                        ¿Cómo bloqueó el candado?
                    </h5>
                    <p>
                        El bloqueo es manual. El candado cuenta con una palanca que está sobre la superficie
                        frontal, esa palanca se debe presionar hacia abajo hasta que la bicicleta quede
                        asegurada y bloqueada.
                    </p>
                    <h5>
                        ¿En donde debo dejar la bicicleta?
                    </h5>
                    <p>
                        Este es un programa de bicicletas compartidas, su responsabilidad es dejar la bicicleta
                        en alguna de las 13 estaciones indicadas en el mapa.
                    </p>
                    <h5>
                        ¿Si no veo bicicletas en el mapa que debo hacer?
                    </h5>
                    <p>
                        Probablemente todas las bicicletas se encuentren en préstamo, pero si usted ve que
                        alguna bicicleta está en una estación con su respectivo casco y puede usarse, debe
                        cerrar el candado y luego aparecerá disponible en el mapa para que usted pueda
                        desbloquearla con su usuario.
                    </p>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
