
<nav id="header" class="navbar navbar-expand-md navbar-dark shadow-sm  badge-dark">
    <div class="container">

        <a class="navbar-brand" href="{{ url('/') }}">

            <img src="/img/potosi_logo_1.png"width="auto" height="60px" class="d-inline-block align-top" alt="">

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                @if(Auth::user()->isAdmin())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('users') }}">{{ __('Usuarios') }}</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('roles') }}">{{ __('Roles') }}</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('bikes') }}">{{ __('Bicicletas') }}</a>
                </li>
                
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('stations') }}">{{ __('Estaciones') }}</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rides') }}">{{ __('Préstamos') }}</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('reports') }}">{{ __('Reportes') }}</a>
                </li>
                
                
                @else
                <li class="nav-item">
                    <a class="nav-link"   href="{{ route('user_user_edit') }}"  >{{ Auth::user()->name}}  {{ Auth::user()->last_name }}</a>
                </li>
                @endif



                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ayuda') }}">{{ __('message.ayuda_title') }}</a>
                </li>

                @guest

                @else

                <li class="nav-item">


                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>


                </li>




                @endguest
            </ul>
        </div>
    </div>
</nav>

