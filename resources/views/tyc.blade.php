@extends('layouts.app' )

@section('content')
<div  class="container mt-5 mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header h1">  
                    Términos y condiciones para el uso del programa
                </div>

                <div class="card-body">
                    <h2>
                        La Pradera en Bicicleta
                    </h2>

                    <p>
                        Le damos la bienvenida a este servicio creado para su comodidad y bienestar.
                    </p>
                    <p>
                        Por favor lea atentamente estos términos y condiciones de uso para que comprenda el objeto, alcance del sitio/App y las reglas sobre el ingreso y manejo del mismo.
                    </p>
                    <p>
                        Al utilizar y/o acceder a cualquier sección de la aplicación móvil y/o sitio web de La Pradera en Bicicleta, se considera que usted, de ahora en adelante “Usuario”, está obligado a: 1.) Cumplir estos términos y condiciones, 2.) Nuestra Política de privacidad y de tratamiento de datos personales y (3.) Nuestra Política de cookies. 
                    </p>
                    <p>
                        Se entiende que el usuario ha aceptado y comprendido todos los términos y condiciones desde el momento en el que ingresa y se registra en esta APP. Lea atentamente todas las condiciones generales, si decide no aceptarlas, no ingrese a la aplicación móvil.
                    </p>

                    <h3>
                        Objeto del servicio:  
                    </h3>

                    <p>
                        El programa La Pradera en bicicleta es un servicio que hace parte de la Oferta de Valor de La Pradera de Potosí que permite a todos los grupos de interés (socios, invitados, colaboradores del Club, Agrupación, proveedores, contratistas, colaboradores de las unidades privadas y visitantes en general) tener acceso a bicicletas de uso compartido para la movilidad interna.
                    </p>
                    <p>
                        El usuario conoce que este servicio común de préstamo de bicicletas (de ahora en adelante “El Servicio”), cuenta con una red de estaciones y/o centros de acopio de bicicletas ubicados y distribuidos en diferentes puntos al interior de La Pradera y que son éstos los únicos autorizados para tomar o regresar la bicicleta. 
                    </p>
                    <p>
                        Además, el usuario conoce que el servicio prestado por La Pradera se realiza por medio de la aplicación móvil y/o página web de propiedad del prestador del servicio. 
                    </p>

                    <h3>
                        Requisitos de acceso al servicio: 
                    </h3>

                    <p>
                        El usuario que accede al sistema, lo hace de forma totalmente voluntaria y asume su responsabilidad directa durante el tiempo de préstamo, uso, desplazamiento, devolución de los elementos, su seguridad y la de otras personas, bienes y demás elementos que hagan parte de las zonas privadas o de las zonas comunes.
                    </p>
                    <p>
                        Usted podrá acceder y usar este Sitio/App y Servicio haciendo el respectivo registro y cumpliendo con la normatividad establecida para el uso del programa. Adicionalmente, el usuario declara que tiene la capacidad física y mental para acceder al servicio y sabe cómo manejar las bicicletas, si el usuario es menor de edad, los padres son los únicos responsables de verificar la idoneidad, habilidades y capacidad para que pueda acceder al servicio. Si usted no cuenta con la capacidad requerida para acceder y usar este servicio, sitio/App, por favor abstenerse de usarlo.

                    </p>
                    <h3>
                        Gestión autónoma de cuenta. 
                    </h3>

                    <p>
                        La Pradera permite a todos sus usuarios elegir su nombre de usuario y contraseña, es responsabilidad del usuario mantener dicha información en la más estricta confidencialidad, puesto que es el único responsable de las operaciones efectuadas dentro de su cuenta. Si en cualquier momento, el usuario considera que un tercero conoce su nombre de usuario y/o contraseña, deberá cambiarlos inmediatamente a través de la Aplicación móvil o de la Administración del Club. En caso de olvidar parte o toda su combinación, por favor póngase en contacto con nosotros. Si el usuario extravía, olvida, pierde o comparte con un tercero su nombre de usuario y/o su contraseña o es incapaz de acceder al Sitio/App o a cualquiera de los Servicios por un motivo distinto de un error de La Pradera, la misma no será ni se hará responsable de reclamación alguna respecto a esa cuenta.
                    </p>

                    <h3>
                        Cancelación y/o suspensión de cuenta. 
                    </h3>
                    <p>
                        La Pradera se reserva el derecho, en todo momento y a su entera discreción, a i) negarse a abrir una Cuenta y/o cerrar o suspender una Cuenta ya existente por incumplimiento al reglamento o si considera que existen razones fundadas para pensar que una cuenta se utiliza, se ha utilizado o se puede utilizar para propósitos ilegales, fraudulentos, deshonestos, o si se ha violado alguna disposición de los presentes Términos y Condiciones.
                    </p>
                    <h3>
                        Obligaciones de La Pradera. 
                    </h3>

                    <p>
                        Se obliga y se compromete en todo momento y durante la operación de la página web y/o aplicación móvil a: i) administrar y gestionar el soporte lógico, operativo y de programación para el acceso a las cuentas de registro de los usuarios, ii) permitir el acceso a las distintas herramientas de La Pradera en Bicicleta dependiendo del tipo de servicio que haya elegido utilizar cada usuario, iii) prestar mantenimiento a la aplicación móvil cuando esta lo requieran, iv) prestar soporte y asistencia a los usuarios sobre el uso de todas las utilidades dispuestas por La Pradera; v) entregar al usuario bicicletas funcionales para su uso, vi) prestar servicio de acopio de las bicicletas en las respectivas estaciones durante los horarios de funcionamiento establecidos.
                    </p>
                    <p>
                        El acceso e ingreso a este Sitio/App implica que usted ha aceptado que el uso que usted hará de este Sitio/App, de sus Contenidos y la información contenida en éste, tendrá propósitos legítimos y legales, y se hará en cumplimiento de estos términos y condiciones y de todas y cualesquiera leyes aplicables.
                    </p>
                    <p>
                        Entre otras consideraciones, usted acepta que no usará este Sitio/App, sus Contenidos o la información contenida en éste, para: (a) transmitir a terceros o de cualquier manera publicar información que es falsa, perjudicial, o respecto de la cual no se cuenta con las debidas autorizaciones legales o contractuales; (b) ocasionar daño a menores de edad o promover o efectuar daños físicos o materiales a cualquier persona o grupo de personas naturales y jurídicas, o a animales; (c) utilizar la identidad o la información personal de personas (naturales o jurídicas) mencionadas en el sitio, para cualquier propósito o finalidad; (d) transmitir o emitir material que contenga virus informáticos o cualquier otro código, programa de computador o aplicación destinada a interrumpir, destruir, restringir o perjudicar la funcionalidad de computadores, programas de computador, sistemas de información, redes de telecomunicaciones o infraestructura y servicios de terceros; (e) de manera intencionada o sin intención violar o incumplir cualquier ley aplicable nacional, local, estatal o internacional, incluyendo pero sin limitarse a las normas de privacidad y protección de datos; (f) recolectar, guardar y administrar datos personales sobre personas naturales y jurídicas sin la correspondiente autorización y en incumplimiento de las leyes aplicables; (g) ejecutar, planear, armar, estructurar o realizar prácticas o actividades de carácter criminal; (h) infringir los derechos de propiedad intelectual de La Pradera en Bicicleta, entre otras conductas lesivas de terceros o de las leyes aplicables; i) alterar, consolidar, modificar, adaptar, descompilar, desmontar o reducir el contenido, servicios, herramientas o utilidades de La Pradera en Bicicleta salvo pacto en contrario; j) contravenir la buena imagen y reputación de La Pradera en Bicicleta.
                    </p>


                    <h3>
                        Uso del servicio de bicicletas 
                    </h3>

                    <p>
                        Con la aceptación de los presentes términos y condiciones, el usuario acepta que: i) La Pradera presta un servicio privado y automatizado de alquiler y préstamo eminentemente personal de bicicletas ii) La Pradera cuenta con espacios autorizados para devolución de los vehículos funcionales en los horarios indicados la aplicación móvil o en el Reglamento del sistema; iii) comprende y ha leído las instrucciones de bloqueo y desbloqueo del sistema candados en cualquier área de prestación del servicio; iv) La Pradera no tiene la obligación de proveer los elementos de seguridad necesarios para el uso de los vehículos. Sin embargo, realizará el préstamo del caso, como elemento de protección personal y de seguridad, el cual es indispensable para transitar al interior de La Pradera de Potosí. v) es responsabilidad del usuario cualquier sobrecosto generado por daños, averías, pérdida, ocasionados por no entregar la bicicleta dentro de los lugares y horarios estipulados y/o por el mal bloqueo y desbloqueo de las mismas, así como por su abandono. Si no se llegase a entregar la bicicleta dentro del horario estipulado, es obligación del usuario la guarda y protección de la misma hasta el día en que se devuelva dicho medio de transporte en la estación o directamente en el cuarto de bicicletas. La entrega del vehículo deberá realizarse como máximo dentro de las 24 horas siguientes al inicio del servicio. Si el usuario no llegase a entregar el vehículo dentro del tiempo indicado, La Pradera iniciará la respectiva acción administrativa de contacto del usuario y reporte al Comité encargado para seguimiento del caso; vi) al momento de recibir, desbloquear y desconectar la bicicleta el usuario debe asegurarse de que el artefacto se encuentra en condiciones adecuadas para su uso. Si el usuario encuentra alguna avería, fallas mecánicas, este deberá contactar a la Administración para efectuar el debido reporte; vii) se abstiene de manipular, desarticular, sustraer, reemplazar, desinstalar, desajustar, descomponer o desmontar la bicicleta y cualquiera de los dispositivos adherido a él; viii) es obligación del usuario cuidar y mantener en buen estado la bicicleta recibida, respondiendo así por cualquier daño o deterioro generado al bien por razones distintas al uso corriente del día a día; ix) es obligación del usuario restituir la bicicleta y sus equipos adheridos en el mismo estado en el que los recibió; x). En el evento de pérdida de la bicicleta, el usuario deberá tramitar el día del hecho la respectiva denuncia ante la autoridad competente, si el usuario se niega, se rehúsa o no llegase a tramitar la respectiva denuncia, La Pradera se reserva el derecho de hacer el respectivo cobro del precio del vehículo a través del medio de pago registrado por el usuario; xi) responde por todos los daños que se causen a terceros con el uso del vehículo, indemnizando y reembolsando toda clase de gastos en que incurra La Pradera para sopesar tal supuesto de hecho. Dentro de dicho rubro se reconocen honorarios de abogados en un evento de reclamación o demanda interpuesta por un tercero en contra de La Pradera, reparación de perjuicios sufridos por La Pradera, gastos por perjuicios causados a terceros o a bienes de su propiedad, y los demás que sean procedentes legalmente; xii) conoce y comprende las normas de tránsito relacionadas con el uso de bicicletas, Cualquier multa generada por el incumplimiento de dicha normativa será responsabilidad exclusiva del usuario; xiii) se abstiene de hacer uso de las bicicletas  para fines ilícitos; xiv) con la prestación del presente servicio, La Pradera no le está transfiriendo bajo ningún título al usuario la propiedad del vehículo, y por ello el usuario no tiene el derecho de transferir, ceder, vender, enajenar ni gravar el vehículo por ningún motivo; xv) Este servicio es gratuito, por  lo tanto el no hará ningún pago ni reconocimiento económico por usar las bicicletas. xvi) en caso de mal funcionamiento del vehículo, avería, rotura, desarreglo, pinchazo, o cualquier eventualidad que impida la normal movilización del artefacto, se deberá contactar con la oficina de servicios o el cuarto de bicicletas de La Pradera para hacer el respectivo acopio del vehículo. xvi) El usuario se abstiene de abandonar la bicicleta en cualquier espacio común o privado sin haber seguido las instrucciones proporcionadas por el personal de La Pradera. Asimismo, se abstiene de intentar arreglar la avería o solventar el problema ocurrido en el debido caso. La Pradera se reserva el derecho de investigar la causa de la avería o falla del vehículo. Si La Pradera determina que el origen de la falla fue por incorrecto uso del artefacto, se procederá a realizar el cobro. xvii) el usuario es libre de movilizar el vehículo por donde desee, pero el vehículo deberá ser debidamente entregado, acoplado y bloqueado en los puntos autorizados ubicados en el aplicativo móvil. 
                    </p>
                    <p>
                        No declaraciones ni garantías: El usuario del Sitio/App reconoce que el Sitio/App y/o la prestación del Servicio de Uso de Bicicletas/Patinetas puede no estar disponible debido a un número de factores, incluyendo mas no limitado a causas de fuerza mayor, acceso no autorizado, virus informáticos, negación del servicio y otros ataques, fallas técnicas del servidor, fallas en la infraestructura de telecomunicaciones o discontinuidad. 
                    </p>
                    <p>
                        No responsabilidad por reclamaciones o daños: Usted como usuario del Servicio prestado por La Pradera a través de su Sitio/App asume su propio riesgo al acceder a éste y usarlo, incluyendo el riesgo personal y de sus propiedades y las de terceros que pueda surgir por conocer, usar, compartir o bajar cualquier Contenido proporcionado en este Sitio/App o como consecuencia de acceder al Uso del Servicio de cualquier otra manera obtenido por usted a través de este Sitio/App. 
                    </p>
                    <p>
                        La Pradera  no reconoce ni acepta responsabilidad alguna por los daños y/o pérdidas a un usuario y/o una tercera parte causados directa y/o indirectamente por motivos de fallo del sistema informático central de  la pradera en bicicleta o de cualquiera de sus componentes; retrasos, pérdidas, errores u omisiones derivados del fallo de cualquier sistema de telecomunicaciones o cualquier otro sistema de transmisión de datos y/o suspensiones del suministro de los Servicios de nuestro Sitio/App originados en fallas técnicas u operativas ajenas a nuestra voluntad, ni de aquellas que escapen de nuestro control tales como cortes de energía eléctrica, ataques informáticos, fallas en los equipos o programas de computación, o en general por eventos de fuerza mayor o caso fortuito. Si la participación del usuario en los Servicios se viera interrumpida por algún fallo en el sistema de telecomunicaciones o del sistema informático del usuario que le impida seguir utilizando los Servicios, La Pradera adoptará todas las medidas razonables y posibles para garantizar que su sistema informático permita al usuario reanudar su participación en los Servicios tal como estaba en el momento inmediatamente anterior a la interrupción.
                    </p>
                    <p>
                        Derechos de propiedad intelectual e industrial: Todos los contenidos, elementos e información de este Sitio/App, incluyendo todo texto, formato, imágenes, música, marcas, logotipos, enseñas, nombres comerciales, sonidos, gráficos, videos, animación, y demás materiales de este Sitio (los “Contenidos”) son de propiedad de La Pradera, sus filiales y afiliadas y controlantes, y aquellos de terceros contratistas, licenciantes o cedentes que corresponda. 
                    </p>
                    <p>
                        Interpretación: Si cualquier sección o aparte de estos términos y condiciones no se puede aplicar o resulta inválida, en su totalidad o en parte, bajo cualquier ley, o sea sentenciada como tal por decisión judicial, dicha parte será interpretada de conformidad con la ley aplicable y su falta de aplicabilidad o invalidez no hará que estos términos y condiciones en general y las disposiciones remanentes o porciones de ellas sean inaplicables o inválidas o ineficaces en su totalidad. En tal evento, las disposiciones serán cambiadas e interpretadas de tal manera que se logren de la mejor manera posible los objetivos de tales disposiciones no aplicables o inválidas, dentro de los límites de la ley aplicable.
                    </p>
                    <p>
                        Modificaciones: Estos términos y condiciones pueden cambiar, por lo tanto La Pradera se reserva el derecho de modificar, cambiar o terminar estos términos y condiciones en cualquier momento y bajo su total discreción, sin necesidad de notificarle a usted previamente.
                    </p>
                    <p>
                        Modificaciones por el usuario: Ninguna sección de estos términos y condiciones podrá ser modificada, suprimida o agregada por el usuario del Sitio/App unilateralmente. 
                    </p>

                    <ul>
                        <li>
                            Preguntas frecuentes 
                        </li>
                        <li>
                            Protocolo y teléfonos de emergencia.
                        </li>
                    </ul>

                    <br>
                    <p>
                        En caso de emergencia mantenga la calma, intente las siguientes opciones:
                    </p>
                    <p>
                        Comuníquese a los siguientes teléfonos:
                    </p>
                    <ul>
                        <li>
                            <strong>Oficina de servicios:</strong> 3123469444 – 3182870744 

                        </li>
                        <li>
                            <strong> Cuarto de Control: </strong> 3182870754 (24 horas)
                        </li>
                    </ul>

                    <p>
                        Diríjase al colaborador más cercano
                    </p>
                    <p>
                        Acérquese a una de las sedes del Club donde los colaboradores le brindarán la ayuda necesaria.

                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
