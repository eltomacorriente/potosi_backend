<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>La Pradera En Bici</title>




        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="js/libraries/leaflet/leaflet.css" />
    </head>
    <body id = "body_map" >
        

        @include('components.header')



        @yield('content')



        <!-- Scripts -->

        <script src="js/libraries/leaflet/leaflet.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        

    </body>
</html>
