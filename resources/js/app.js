/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

var rider_ant = true;
var rider_new = false;

require('./bootstrap');
require('./mapa');


window.Vue = require('vue');

window.laravel_tocken = document.head.querySelector('meta[name="csrf-token"]').content;

Vue.component('counter', require('./components/Counter.vue').default);
Vue.component('my-map', require('./components/MapFrame.vue').default);
Vue.component('security-questions', require('./components/SecurityFrame.vue').default);
Vue.component('user-table', require('./components/UserTable.vue').default);
Vue.component('role-table', require('./components/RoleTable.vue').default);
Vue.component('bike-lock-table', require('./components/BikeLockTable.vue').default);
Vue.component('station-table', require('./components/StationTable.vue').default);

// Rides
Vue.component('active-rides-table', require('./components/rides/ActiveRidesTable.vue').default);
Vue.component('ended-rides-table', require('./components/rides/EndedRidesTable.vue').default);
Vue.component('ride-map', require('./components/rides/RideMap.vue').default);
Vue.component('ride-by-user', require('./components/rides/RidesByUserTable.vue').default);

// Reportes
Vue.component('report-by-day', require('./components/reports/RidesByDay.vue').default);

app = 's';
app.state = 2;


window.registro_select = function (yo) {    

    var x = document.getElementById("categorySelect").value;
    if(x == 2){
        document.getElementById("label_category_information").textContent = "¿ De cuál club?";
        document.getElementById("category_information").required = true;
        document.getElementById("category_information").disabled = false;
    } else if(x == 4){
        document.getElementById("label_category_information").textContent = "¿Ingresa el número de Vivienda?";
        document.getElementById("category_information").required = true;
        document.getElementById("category_information").disabled = false;
    }else {
         document.getElementById("label_category_information").textContent = "";
        document.getElementById("category_information").required = false;
        document.getElementById("category_information").disabled = true;
    }
    
    console.log(x);
}

window.mainController = function () {
    // Revisar que este todo bien
    axios.get("/hearthbeat").then(res => {
        hearthbeat = res.data;
        window.printBikes(hearthbeat.enabled_bikes);
        window.printRides(hearthbeat.active_rides);
    });
};

//window.mainController();

window.incrementCount = function () {
    setInterval(function () {
        window.mainController();
    }, 10000);
};




try {
    var app = new Vue({
        el: '#app',
        props: ['user'],
        data: {state: '2', message: 'hola', msj: "j"},
        template: '<div class="flexy">' +
                '<div id="mapContainer" v-if="state == 2" >' +
                ' <my-map></my-map> ' +
                ' </div>' +
                ' <div  v-else> ' +
                '  ' +
                ' <counter :msj="message" > </counter> ' +
                ' </div> </div>'
        ,
        mounted() {

        },
        functions: {
            hola: function () {
                window.mainController();
                window.incrementCount();
            }
        }

    });
} catch (error) {

}

try {
    var userTable = new Vue({
        el: '#vue-table',
    });

} catch (error) {

}
