
window.MiMapa = class {

    constructor() {
        if (document.getElementById('map') !== null) {
            MiMapa.map = new L.Map('map');

        } else {
            throw "NO SE CARGO EL MAPA. No existe el elemento map";
        }
         //MiMapa.map = new L.Map('map');
    }

    init() {
        this.chargeLayer();
        this.GoToIntialPosition();
        this.chargeIcons();
        
        this.initHome();
        this.initRides();
        this.initStations();
        this.initBikes();
    }

    chargeLayer() {
        var BlackWhite = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
        });

        var wikimedia =
                L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors. Base map: <a href="https://www.mediawiki.org/wiki/Maps#Production_maps_cluster">wikimedia maps</a>',
                    maxZoom: 19
                });

        var toner =
                L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors. Base map: <a href="http://maps.stamen.com/toner/">toner by stamen</a>',
                    maxZoom: 18
                });

        var defaultMap =
                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
                    maxZoom: 20
                }).addTo(MiMapa.map);


        var baseMaps = {
            'Wikimedia maps': wikimedia,
            'Stamen toner': toner,
            'BlackWhite': BlackWhite,
            'Default': defaultMap,
        };
        var layerControl = L.control.layers(baseMaps).addTo(MiMapa.map);



    }

    GoToIntialPosition() {
        var potosi = new L.LatLng(4.793723762231986, -73.9617659069909);
        MiMapa.map.setView(potosi, 15);
    }

    chargeIcons() {
        var RedIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/bici2-r.png',
                iconRetinaUrl: '../images/bici2-r.png'
            }});
        var GreenIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/bici2-g.png',
                iconRetinaUrl: '../images/bici2-g.png'
            }});
        var BlueIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/bici2-b.png',
                iconRetinaUrl: '../images/bici2-b.png'
            }});
        var YellowIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/bici2-y.png',
                iconRetinaUrl: '../images/bici2-y.png'
            }});
        var AdultoIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/adulto.png',
                iconRetinaUrl: '../images/adulto.png',
                iconSize: [ 50, 32 ],
            }});
        var NinoIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/nino.png',
                iconRetinaUrl: '../images/nino.png',
                iconSize: [ 50, 34 ],
            }});
        var InhabilitadaIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/inhabilitada.png',
                iconRetinaUrl: '../images/inhabilitada.png',
                iconSize: [ 50, 50 ],
            }});
        var EstacionIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/estacion.png',
                iconRetinaUrl:  '../images/estacion.png',
                 iconSize: [ 50, 70 ],
                 iconAnchor: [ 16, 63 ],
            }});
        var HomeIcon = L.Icon.Default.extend({options: {
                iconUrl: '../images/home.png',
                iconRetinaUrl:  '../images/home.png',
                 iconSize: [ 50, 48 ], 
                 iconAnchor: [ 25, 24 ],
            }});
        this.redIcon = new RedIcon();
        this.greenIcon = new GreenIcon();
        this.yellowIcon = new YellowIcon();
        this.blueIcon = new BlueIcon();
        
        this.adultoIcon = new AdultoIcon();
        this.ninoIcon = new NinoIcon();
        this.inhabilitadaIcon = new InhabilitadaIcon();
        this.estacionIcon = new EstacionIcon();
        this.homeIcon = new HomeIcon();
        
        
    }
    invalidateSize(){
         MiMapa.map.invalidateSize();
    }
    
    initHome(){
       var latitude = 4.793708768149641;
       var longitude =  -73.95508938820122;
       var text = "Cuarto de Bicicletas";
       
        this.homeMarkers =
                new L.marker([latitude, longitude], {icon: this.homeIcon}).addTo(MiMapa.map);
        this.homeMarkers.bindPopup(text);
       
    }
    
    initStations() {
        this.stationMarkers = [];
        stationMarkersLength = 0;
    }

    printStation(latitude, longitude, text) {
        
         this.stationMarkers =
                new L.marker([latitude, longitude], {icon: this.estacionIcon}).addTo(MiMapa.map);
        this.stationMarkers.bindPopup(text);
        
        
        //MiMapa.map.addLayer(circle);
    }

    resetStations() {

    }

    printStations(mapMarkers) {

        for (mm in mapMarkers) {
            var element = mapMarkers[mm];
            this.printStation(element.latitude, element.longitude, element.name);
        }

    }

    initBikes() {
        this.bikeMarkers = [];
        bikeMarkersLength = 0;
    }

    resetBikes() {
        for (i = 0; i < this.bikeMarkersLength; i++) {
            this.bikeMarkers[i].remove();
        }
        this.bikeMarkersLength = 0;
    }

    printBike(latitude, longitude, icon, text) {
        var index = this.bikeMarkersLength++;
        this.bikeMarkers[index] =
                new L.marker([latitude, longitude], {icon: icon}).addTo(MiMapa.map);
        this.bikeMarkers[index].bindPopup(text);
    }

    printBikes(mapMarkers) {

        this.resetBikes();

        for (mm in mapMarkers) {
            var element = mapMarkers[mm];
            var icon;
            var recomendationText = "";
            switch (element.model) {
                case 'Adulto':
                    icon = this.adultoIcon;
                    recomendationText = "Recomendada para mayores de 12";
                    break;
                case 'Niño':
                    icon = this.ninoIcon;
                    recomendationText = " Recomendada para menores de 11 años";
                   
                    break;
                default :
                    icon = this.inhabilitadaIcon;
                    break;
            }
            
           text     =    " Bicicleta : " + element.name + " <br> " +  
                " Modelo : " + element.model + " <br> " +  
               " Candado : " + element.qr_code + " <br> <br>  " +  
                " <h6> " + recomendationText + " </h6> " ; 
            this.printBike(element.latitude, element.longitude, icon, text);

        }
    }

    initRides() {
        this.rideMarkers = [];
        rideMarkersLength = 0;
    }

    printRide(ride) {
        //element.id, element.points, text)
        ride_id = ride.id;
        points = ride.points;
        ride_id = ride.id;
        lock = ride.lock; 
        var polylineOptions = {
            color: 'rgb(219,43,153)',
            weight: 2,
            opacity: 0.9
        };

        var polylinePoints = [];

        for (i in points) {
            polylinePoints.push(new L.LatLng(points[i].latitude, points[i].longitude));
        }

        var index = this.rideMarkersLength++;
        this.rideMarkers[index] = new L.Polyline(polylinePoints, polylineOptions);
        MiMapa.map.addLayer(this.rideMarkers[index]);
        text     = " Numero de Viaje : " + ride_id + " <br> " +
                " Bicicleta : " + lock.bike.name + " <br> " +  
                " Candado : " + lock.qr_code + " <br> " +  
                " Fecha Inicio : " + ride.departure_time + " <br> " ;
        this.printBike(lock.last_point.latitude, lock.last_point.longitude, this.redIcon, text);
      
    }

    resetRides() {
        for (i = 0; i < this.rideMarkersLength; i++) {
            this.rideMarkers[i].remove();
        }
        this.rideMarkersLength = 0;
    }

    printRides(mapMarkers) {
        this.resetRides();
        for (mm in mapMarkers) {
            var element = mapMarkers[mm];
           
            this.printRide(element);
            /*
             * IMPRIMIR EL ULTIMO PUNTO COMO LA POSCIICON DEL CANDADO
             * if (element.points.length > 0) {
             last_point = element.points.pop();
             this.printBike(last_point.latitude, last_point.longitude, this.redIcon, text);
             }*/

            // Posicion el candado
        }


    }

};