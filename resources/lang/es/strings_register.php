<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Register Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    Las siguientes lineas están relacionadas con el formulario de registro
    |
    */
    'canje' => 'Canje',
    'invitado' => 'Invitado',
    'colaborador' => 'Colaborador de vivienda',
    'independiente' => 'Independiente',
    
    'Logout' => 'Cerrar Sesión',
    
    'title' => 'Formulario de Registro',
    'birthdate' => 'Fecha de Nacimiento',
    'phone_number' => 'Número de Teléfono',
   
];