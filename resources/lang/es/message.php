<?php
return [
  // mensajes de ayuda
    'ayuda_title'   => 'Ayuda',    
    'ayuda_text_1'   => 'Este es el programa de bicicletas en la Pradera de Potosí!',
    'ayuda_text_2'   => 'Cualquier duda comunicate al :ayuda_number ',
    'ayuda_number'   => '1235',
    
];