<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
  }); */
/*
  Route::get('/user', function (Request $request) {
  return $request->user();
  });
 */
Route::apiResource('stations', 'Api\StationController');
Route::apiResource('users', 'Api\UserController');
/*
Route::apiResource('bikes', 'Api\BikeController');
Route::apiResource('locks', 'Api\LockController');
Route::apiResource('points', 'Api\PointController');
Route::apiResource('rides', 'Api\RideController');
Route::apiResource('roles', 'Api\RoleController');
Route::apiResource('stations', 'Api\StationController');
Route::apiResource('users', 'Api\UserController');

*/
//Route::apiResource('roles', 'RoleController');
//Route::middleware('auth:api')->get('/isRider/{id}', 'RentController@isRider');





// Rent Controllers

// nop
//Route::get('/isRider/{id}', 'Rent\RentController@isRider');

// Start Ride
//Route::get('/startRide/{bike_number}', 'Rent\StartController');

// End Ride
//Route::get('/endRide/{ride_id}', 'Rent\EndController');

// hearthbeat API
//Route::get('/hearthbeat', 'Rent\HearthBeatApiController');
