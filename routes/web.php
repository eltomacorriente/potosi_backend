<?php

/**
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  | php artisan make:controller LockController --resource
 */
use Illuminate\Support\Facades\Auth;

/*
  Route::resource('bikes', 'BikeController');
  Route::resource('locks', 'LockController');
  Route::resource('points', 'PointController');
  Route::resource('rides', 'RideController');
  Route::resource('roles', 'RoleController');
  Route::resource('stations', 'StationController');
  Route::resource('users', 'UserController');
 */

//Route::resource('bikes', 'BikeController');
//
// show station table
Route::get('/recalculate_distance', 'Admin\RidesController@recalculateDistance');



Route::get('/', function () {
    if (Auth::check()) {
        return redirect('map');
    } else {
        return view('welcome');
    }
})->name('/');

Route::get('/ayuda', function () {
    return view('ayuda');
})->name('ayuda');

Route::get('/tyc', function () {
    return view('tyc');
})->name('tyc');


Auth::routes();

Route::get("/error", function() {
    return view('error');
})->name('error');


// Api Error
Route::get("/error", function() {
    return view('error');
})->name('api_error');

Route::get('/map', 'HomeController@index')->name('map');

Route::get('/home', function () {
    return redirect('map');
})->name('home');



// Lock Commands
// When a lock is closed
Route::get('lock/wasClosed/{imei}', 'Lock\WasClosedController');

// When a lock is opened
Route::get('lock/wasOpened/{imei}', 'Lock\WasOpenedController');

// When a lock send a hearthbeat
Route::get('lock/hearthbeat/{imei}/{battery_level}/{status}/{gsm_signal}', 'Lock\HearthbeatController');

// When a lock send a location
Route::get('lock/location/{imei}/{latitude}/{longitude}/{date}', 'Lock\LocationController');



Route::middleware('auth')->group(function() {
// Rent Controllers
// Start Ride
    Route::get('/startRide/{bike_number}', 'Rent\StartController');

// End Ride
    Route::get('/endRide/{ride_id}', 'Rent\EndController');

// hearthbeat API
    Route::get('/hearthbeat', 'Rent\HearthBeatApiController');

// User features    
     Route::prefix('user')->group(function() {
         
        // Show form to edit user
            Route::get('/edit', 'User\UserController@edit')->name('user_user_edit');

// edit user
            Route::post('/update', 'User\UserController@update')->name('user_user_update');


        // change password controller
         Route::post('/password', 'User\ChangePasswordController')->name('change_password');
        
            
     });
    
    
    
// ADMIN CONTROLLERS

    Route::prefix('admin')->group(function() {

// User commands
        Route::prefix('users')->group(function() {

// show user table
            Route::get('/', function() {
                return view('admin/users');
            })->name('users');

// Show form to edit user
            Route::get('/{user}', 'Admin\UserController@edit')->name('user_edit');

// edit user
            Route::post('/{user}/update', 'Admin\UserController@update')->name('user_update');


// Get All users
            Route::post('/', 'Admin\UserController@index');

// Enable an User
            Route::post('/enable/{user}', 'Admin\UserController@enable');

// Disble an User
            Route::post('/disable/{user}', 'Admin\UserController@disable');
            
            // Reset password
            Route::post('/resetPassword/{user}' , 'Admin\UserController@resetPassword')->name('user_reset_password');

        });

// Roles commands    
        Route::prefix('roles')->group(function() {

// show role table
            Route::get('/', function() {
                return view('admin/roles');
            })->name('roles');

// Get All roles JS
            Route::post('/', 'Admin\RoleController@index')->name('role_index');

// create role
            Route::post('/store', 'Admin\RoleController@store')->name('role_store');

// update role
            Route::post('/{role}/update', 'Admin\RoleController@update')->name('role_update');

// delete role
            Route::post('/{role}/destroy', 'Admin\RoleController@destroy')->name('role_destroy');
        });

// Bikes commands    
        Route::prefix('bikes')->group(function() {

// Get All bikes JS
            Route::get('/api', 'Admin\BikeController@index')->name('bike_index_api');

// show bike table
            Route::get('/', function() {
                return view('admin/bikes_locks');
            })->name('bikes');


// Get All bikes JS
            Route::post('/', 'Admin\BikeController@index')->name('bike_index');

// Show form to edit bike
            Route::get('/{bike}', 'Admin\BikeController@edit')->name('bike_edit');



// create bike
            Route::post('/store', 'Admin\BikeController@store')->name('bike_store');

// update bike
            Route::post('/{bike}/update', 'Admin\BikeController@update')->name('bike_update');

// delete bike
            Route::post('/{bike}/destroy', 'Admin\BikeController@destroy')->name('bike_destroy');

// Enable an User
            Route::post('/enable/{bike}', 'Admin\BikeController@enable');

// Disble an User
            Route::post('/disable/{bike}', 'Admin\BikeController@disable');
        });

// Stations commands    
        Route::prefix('stations')->group(function() {

// show station table
            Route::get('/', function() {
                return view('admin/stations');
            })->name('stations');

// Get All stations JS
            Route::post('/', 'Admin\StationController@index')->name('station_index');

// create station
            Route::post('/store', 'Admin\StationController@store')->name('station_store');

// update station
            Route::post('/{station}/update', 'Admin\StationController@update')->name('station_update');

// delete station
            Route::post('/{station}/destroy', 'Admin\StationController@destroy')->name('station_destroy');

// Enable an User
            Route::post('/enable/{station}', 'Admin\StationController@enable');

// Disble an User
            Route::post('/disable/{station}', 'Admin\StationController@disable');
        });

// toOpen a lock
        Route::post('locks/openLock/{id}', 'Lock\OpenLockController');

// Rides commands    
        Route::prefix('rides')->group(function() {

// show ride view
            Route::get('/', function() {
                return view('admin.rides');
            })->name('rides');
            
            // show ride map
            Route::get('/{ride}', 'Admin\RidesController@show' )->name('ride')->where('ride', '[0-9]+');

            // get active rides
            Route::post('/active', 'Admin\RidesController@active_rides')->name('active_rides');

            // get ended rides
            Route::post('/ended', 'Admin\RidesController@ended_rides')->name('ended_rides');
            
              // get  rides by user
            Route::post('/{user}/user', 'Admin\RidesController@ridesByUser')->name('rides_by_user');
            
        });
        
        // Reports commands    
        Route::prefix('reports')->group(function() {

        // show Reports table
            Route::get('/', function() {
                return view('admin.reports');
            })->name('reports');
            
            // show Reports table
            Route::post('/by_day', 'Admin\ReportController@ridesByDay')->name('reports_by_day');
           
        });
        
        
    });
});
