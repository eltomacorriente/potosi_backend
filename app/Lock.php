<?php

namespace Potosi;

use Illuminate\Database\Eloquent\Model;
use Potosi\Point;

class Lock extends Model {

    const UPDATED_AT = 'date_status';

    public function point() {
        return $this->hasMany('Potosi\Point' , 'lock_id' , 'id');
    }
    
    public function bike() {
        return $this->belongsTo('Potosi\Bike' , 'bike_id' , 'id');
    }
    
    //return Point::orderBy('points.created_at','desc')->find($this->point_id); 
    public function lastPoint(){
        return $this->belongsTo('Potosi\Point' , 'point_id' , 'id');
    }
    
    
    
    public function open() {
        $imei = $this->imei;
        $url = "http://potosibikesharing.tk/openLock.php?imei=" . (int) $imei;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($remote_server_output)->success == "true";
    }
    
    


    public function scopeImei($query , $imei){
        return $query->where('imei', $imei);
    }
    
    public function setStatus($status_dev){
        if($status_dev == 0){
            $this->setOpened();
        }else{
            $this->setClosed();
        }
    }
    
    public function setOpened(){
        $this->status = 'open';
    }
    
    public function setClosed(){
        $this->status = 'close';
    }
    
    public function isOpen(){
        return 'open' == $this->status;
    }
}
