<?php

namespace Potosi;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model {

    public function lock() {
        return $this->belongsTo('Potosi\Lock', 'lock_id', 'id');
    }

    public function scopeEnabled($query) {
        return $query->where('bikes.status', 'enabled');
    }

}
