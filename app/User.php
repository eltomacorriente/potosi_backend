<?php

namespace Potosi;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    private static $category_options = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identification_type',
        'identification',
        'role_id',
        'category_information',
        'name',
        'last_name',
        'email',
        'gender',
        'phone_number',
        'status',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsTo('Potosi\Role', 'role_id', 'id');
    }

    /**
     * Method for knowing if an user is an admin or not
     * 
     * @return Boolean 
     */
    public function isAdmin() {
        return $this->role->name == 'Administrador';
    }

    /**
     * Method for knowing if an user is an admin or not
     * 
     * @return Boolean 
     */
    public function enable() {
        $this->status = 'enabled';
        $this->save();
    }
    
    
    /**
     * Method for knowing if an user is an admin or not
     * 
     * @return Boolean 
     */
    public function disable() {
        $this->status = 'disabled';
        $this->save();
    }

}
