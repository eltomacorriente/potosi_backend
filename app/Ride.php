<?php

namespace Potosi;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ride extends Model {

    const CREATED_AT = 'departure_time';
    const UPDATED_AT = null;

    public function lock() {
        return $this->belongsTo('Potosi\Lock', 'lock_id', 'id');
    }

    public function bike() {
        return $this->belongsTo('Potosi\Role', 'bike_id', 'id');
    }

    public function user() {
        return $this->belongsTo('Potosi\Role', 'user_id', 'id');
    }

    public function points() {
        return $this->hasMany('Potosi\Point', 'ride_id', 'id');
    }

    public function setFinalized() {
        if ($this->status != "Finalized") {
            $this->status = "Finalized";
            $this->arrival_id = $this->lock->point_id;
            $this->arrival_time = Carbon::now();
            $this->time_elapsed = strtotime($this->arrival_time) - strtotime($this->departure_time);
            $this->distance = $this->calculateDistance();
            $this->save();
        }
    }

    public function calculateDistance() {
        $distance = 0;
        
        $points = $this->points;
        $size = sizeof($points);
        if ($size > 0) {
            for ($i = 1; $i < $size; $i++) {               
                $lat1 = $points[$i - 1]->latitude;
                $lon1 = $points[$i - 1]->longitude;
                $lat2 = $points[$i ]->latitude;
                $lon2 = $points[$i]->longitude;
                $dcal = $this->distanceEarthCoordinates($lat1, $lon1, $lat2, $lon2);
                echo ("dist ->" . $dcal . "\n");
                $distance += 1000*$dcal;
            }
        }
        return $distance;
    }

    function degreesToRadians($degrees) {
        return $degrees * pi() / 180;
    }

    function distanceEarthCoordinates($lat1, $lon1, $lat2, $lon2) {
        $earthRadiusKm = 6371;

        $dLat = $this->degreesToRadians($lat2 - $lat1);
        $dLon = $this->degreesToRadians($lon2 - $lon1);

        $lat1 = $this->degreesToRadians($lat1);
        $lat2 = $this->degreesToRadians($lat2);

        $a = sin($dLat / 2) * sin($dLat / 2) +
                sin($dLon / 2) * sin($dLon / 2) * cos($lat1) * cos($lat2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return $earthRadiusKm * $c;
    }

    public function scopeInProgress($query) {
        return $query->where('rides.status', 'In Progress');
    }

    public function scopeFinalized($query) {
        return $query->where('rides.status', 'Finalized');
    }

    public function scopeLock($query, $lock_id) {
        return $query->where('rides.status', $lock_id);
    }

    public function scopeByUser($query, $user_id) {
        return $query->where('rides.status', $user_id);
    }

    public function scopeActive($query) {
        return $query->where('rides.status', 'In Progress');
    }

}
