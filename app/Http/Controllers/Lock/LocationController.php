<?php

namespace Potosi\Http\Controllers\Lock;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Lock ;
use Potosi\Point ;
use Potosi\Ride;

class LocationController extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        
        // traer datos del lock
        //{imei}/{latitude}/{longitude}/{date}
        $lock = Lock::imei($request->imei)->firstOrFail();

        // creo un nuevo point
        $point = new Point();
        
        // llenar point table        
        $point->longitude = $request->longitude;
        $point->latitude = $request->latitude;
        $point->lock_id = $lock->id;
        $point->status = $lock->status;
        $point->battery_status = $lock->battery_status;
        $point->gsm_signal = $lock->gsm_signal;
        
        //esta en Renta?
        $ride = Ride::inProgress()->lock($lock->id)->first();
        if ($ride != null) {
            // si llenar campo
            $point->ride_id = $ride->id;
        }

        
        //$lock->$date = $request->date;
        $point->save();

        // actualizar point del lock
        $lock->point_id = $point->id;
        $lock->save();
    }

}
