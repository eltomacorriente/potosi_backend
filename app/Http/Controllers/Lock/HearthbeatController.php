<?php

namespace Potosi\Http\Controllers\Lock;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Lock;
use Potosi\Ride;

class HearthbeatController extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        // traer datos del lock
        //'lock/hearthbeat/{imei}/{battery_level}/{status}/{gsm_signal}'
        $lock = Lock::imei($request->imei)->firstOrFail();

        // actualizar lock
        $lock->battery_level = $request->battery_level;
        $lock->setStatus($request->status);
        $lock->gsm_signal = $request->gsm_signal;
        $lock->save();
        
        //esta en Renta y cerrado?
         // esta en renta?
        $ride = Ride::inProgress()->lock($lock->id)->first();
        if( $ride != null && !$lock->isOpen() ){
            // si llenar campo
            //llamar metodo para cierre de renta
            $ride->setFinalized();
            $ride->save();
        }
        
    }

}
