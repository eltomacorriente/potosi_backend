<?php

namespace Potosi\Http\Controllers\Lock;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Lock;

class WasOpenedController extends Controller
{
      /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // esta en renta?
        
        // Si llenar el campo en ride
        
        // actualizar estado
        $lock = Lock::imei($request->imei)->firstOrFail();
        $lock->setOpened();
        $lock->save();
        return $lock;
    }
}
