<?php

namespace Potosi\Http\Controllers\Lock;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Lock;
use Potosi\Ride;

class WasClosedController extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {

        // actualizar estado
        $lock = Lock::imei($request->imei)->firstOrFail();
        $lock->setClosed();
        $lock->save();        
        
        // esta en renta?
        $ride = Ride::inProgress()->lock($lock->id)->first();
        if($ride != null){
            // si llenar campo
            //llamar metodo para cierre de renta
            $ride->setFinalized();
            $ride->save();
        }
      
        
        return $lock;
    }

}
