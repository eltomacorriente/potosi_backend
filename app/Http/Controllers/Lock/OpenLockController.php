<?php

namespace Potosi\Http\Controllers\Lock;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Lock as SmartLock;

class OpenLockController extends Controller {
      /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id) {
        $lock = SmartLock::find($id);
        return $lock->open() . "";
    }

}
