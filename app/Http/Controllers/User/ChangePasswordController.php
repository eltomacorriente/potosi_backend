<?php

namespace Potosi\Http\Controllers\User;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Potosi\User;

class ChangePasswordController extends Controller {
    
    
    
    // VAlidator for checking the passwords
    public function admin_credential_rules(array $data) {
        $messages = [
            'current-password.required' => 'Por favor ingrese la constraseña',
            'password.required' => 'Por favor ingrese la constraseña',
        ];

        $validator = Validator::make($data, [
                    'current-password' => 'required',
                    'password' => 'required|same:password',
                    'password_confirmation' => 'required|same:password',
                        ], $messages);

        return $validator;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        if (Auth::Check()) {
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if ($validator->fails()) {
                return redirect()->route('password' ) ->withErrors( $validator->getMessageBag()->toArray());
                
            } else {
                $current_password = Auth::User()->password;
                if (Hash::check($request_data['current-password'], $current_password)) {
                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);                    
                    $obj_user->save();
                    return redirect()->route('/');
                } else {
                    $error = array('current-password' => 'Contraseña incorrecta');
                    return redirect()->route('password' ) ->withErrors( $error);
                }
            }
        } else {
            return redirect()->to('/');
        }
    }

}
