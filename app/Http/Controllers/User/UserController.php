<?php

namespace Potosi\Http\Controllers\User;

use Potosi\User;
use Potosi\Role;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return User::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $user = Auth::user();
        $roles = Role::get();
        return view('user/change_password', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $user = Auth::user();
        if ($user->id == Auth::id()) {

            $user->identification_type = $request->input("identification_type");
            $user->identification = $request->input("identification");
            $user->role_id = $request->input("category");
            $user->category_information = $request->input("category_information");
            $user->name = $request->input("name");
            $user->last_name = $request->input("last_name");
            $user->email = $request->input("email");
            $user->gender = $request->input("gender");
            $user->phone_number = $request->input("phone_number");
            $user->save();
        }
        return redirect()->action('User\UserController@edit');
    }

    /**
     * Enable an user
     *
     * @return \Illuminate\Http\Response
     */
    public function enable(User $user) {
        $user->enable();
        return $user;
    }

    /**
     * Disable an User
     *
     * @return \Illuminate\Http\Response
     */
    public function disable(User $user) {
        $user->disable();
        return $user;
    }

}
