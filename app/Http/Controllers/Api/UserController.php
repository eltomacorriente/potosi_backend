<?php

namespace Potosi\Http\Controllers\Api;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Potosi\User;


class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return User::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->All();

        $validator = Validator::make($data, [
                    'identification_type' => ['required', 'string', 'max:2'],
                    'identification' => ['required', 'integer', 'unique:users'],
                    'category' => ['required', 'integer'],
                    'category_information' => ['nullable', 'string'],
                    'name' => ['required', 'string', 'max:255'],
                    'last_name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'gender' => ['required', 'string', 'max:255'],
                    'phone_number' => ['required', 'string', 'max:255'],
                    'password' => ['required', 'string', 'max:255', 'min:4'],
        ]);

        if ($validator->fails()) {
            return json_encode($validator->getMessageBag()->toArray());
        }
        if (!isset($data['category_information'])) {
            $data['category_information'] = "";
        }
        
        if( $data['category_information'] != "CC" && $data['category_information'] != "TI" && $data['category_information'] != "CE"){
            $data['category_information'] = "CC";
        }
        
        if( $data['category_information'] != "Masculino" && $data['category_information'] != "Femenino" && $data['category_information'] != "Otro"){
            $data['category_information'] = "Masculino";
        }


        return User::create([
                    'identification_type' => $data['identification_type'],
                    'identification' => $data['identification'],
                    'role_id' =>  $data['category'],
                    'category_information' => $data['category_information'],
                    'name' => $data['name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'gender' => $data['gender'],
                    'phone_number' => $data['phone_number'],
                    'status' => 'enabled',
                    'password' => Hash::make($data['password']),
                    'api_token' => Str::random(60),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        $user->save();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $user->delete();
        return User::all();
    }

}
