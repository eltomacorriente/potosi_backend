<?php

namespace Potosi\Http\Controllers\Rent;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Http\Controllers\Rent\HearthbeatUser;
use Illuminate\Support\Facades\DB;
use Potosi\Lock;
use Potosi\Point;
use Illuminate\Support\Facades\Auth;
use Potosi\Bike;
use Potosi\Ride;

class HearthBeatApiController extends Controller {

    public function __construct() {
        //$this->middleware('auth:api');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        // traigo datis del usuario
        $user = Auth::user();
        if ($user->isAdmin()) {
            return app('Potosi\Http\Controllers\Rent\HearthbeatAdmin')->__invoke($request);
        } else {
            return app('Potosi\Http\Controllers\Rent\HearthbeatUser')->__invoke($request);
        }
    }

}
