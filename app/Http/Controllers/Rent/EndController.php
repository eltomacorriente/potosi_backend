<?php

namespace Potosi\Http\Controllers\Rent;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Potosi\Ride;


class EndController extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($ride_id) {
        $ride = Ride::findOrFail($ride_id);
        $ride->setFinalized();
        $ride->save();
        return "true";
    }

    

}
