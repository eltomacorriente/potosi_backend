<?php

namespace Potosi\Http\Controllers\Rent;

use Potosi\Http\Requests\RentRequest;
use Potosi\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Potosi\Bike;

class StartController extends Controller {

    private $bike;

    public function __construct() {
        $this->middleware('auth');
    }

   

    /**
     * Edit DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function createRide() {
        $ride = new \Potosi\Ride();
        $ride->status = "In Progress";
        $ride->user_id = Auth::user()->id;
        $ride->bike_id = $this->bike->id;
        $ride->lock_id = $this->bike->lock->id;
        $ride->departure_id = $this->bike->lock->point_id;
        $ride->save();
    }

    /**
     * Open Lock
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function openLock() {
        $lock = \Potosi\Lock::find($this->bike->lock->id);
        return $lock->open();
    }

    /**
     * Handle the incoming request.
     *
     * @param  Potosi\Http\Requests\RentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RentRequest $request) {
        $this->bike = $request->bike;
        $this->createRide();
        return $this->openLock() . "";
       // return "Numero -> ".$req->input("bike_number");
    }

}
