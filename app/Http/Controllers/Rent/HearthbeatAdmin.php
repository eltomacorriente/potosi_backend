<?php

namespace Potosi\Http\Controllers\Rent;

use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Potosi\Lock;
use Potosi\Point;
use Illuminate\Support\Facades\Auth;
use Potosi\Bike;
use Potosi\Ride;

class HearthbeatAdmin extends Controller {

    public function __construct() {
        //$this->middleware('auth:api');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {

        // preparo respuesta
        $reponse = array();

        //Bicicletas disponibles
        $reponse['enabled_bikes'] = $this->getAllBikes();
        // Traer Viajes Activos
        $reponse['active_rides'] = $this->getAllActiveRides();
        // Puede sacar mas?
        // return Biciclas dispo , bicis prestadas por el usuario , auth
        return $reponse;
    }

    /**
     * get the available bikes
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllBikes() {
        // PRepraro respuesta
        $reponse = array();
        // traigo las bicicletas con su posición
        $bikes = Bike::select(
                        'bikes.*', 'locks.*', 'points.latitude as latitude', 'points.longitude as longitude', 'points.created_at as last_position_date', 'locks.status as lock_status', 'bikes.status as status')
                ->join('locks', 'locks.id', '=', 'bikes.lock_id')
                ->join('points', 'points.id', '=', 'locks.point_id')
                ->enabled()
                ->get();

        // Reviso que no haya prestamos activos con cada bicicleta
      /*  foreach ($bikes as &$line) {
            //traigo el ride
            $ride = Ride::where('bike_id', $line->bike_id)
                    ->where('status', '=', 'In Progress')
                    ->first();
            // si no es nulo lo agrego a la respuesta
            if ($ride == null) {
                array_push($reponse, $line);
            }
        }*/

        return $bikes;
    }

    /**
     * get the active Rides
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllActiveRides() {


        $rides = Ride::active()
                ->get();
        foreach ($rides as $ride) {
            $ride->points;
            $ride->lock->lastPoint;
            $ride->lock->bike;
        }

        // Envio Rta
        return $rides;
    }

}
