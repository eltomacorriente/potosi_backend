<?php

namespace Potosi\Http\Controllers\Admin;

use Potosi\Ride;
use Potosi\User;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;

class RidesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Ride::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Ride $ride) {
        $ride->points;
        $ride->lock->lastPoint;    
        $ride->lock->bike;
        return $ride;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active_rides() {
        return Ride::select(
                                'rides.*', 'bikes.name as bike_name', 'locks.qr_code', 'users.name', 'users.last_name'
                        )
                        ->join('bikes', 'bikes.id', '=', 'rides.bike_id')
                        ->join('locks', 'locks.id', '=', 'rides.lock_id')
                        ->join('users', 'users.id', '=', 'rides.user_id')
                        ->inProgress()
                        ->get();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ridesByUser(User $user) {
        $rides = Ride::select(
                        'rides.*', 'bikes.name as bike_name', 'locks.qr_code', 'users.name', 'users.last_name', 'dep_point.latitude as departure_latitude', 'dep_point.longitude as departure_longitude', 'arr_point.latitude as arrival_latitude', 'arr_point.longitude as arrival_longitude'
                )
                ->join('bikes', 'bikes.id', '=', 'rides.bike_id')
                ->join('locks', 'locks.id', '=', 'rides.lock_id')
                ->join('users', 'users.id', '=', 'rides.user_id')
                ->join('points as dep_point', 'dep_point.id', '=', 'rides.departure_id')
                ->join('points as arr_point', 'arr_point.id', '=', 'rides.departure_id')
                ->where('rides.user_id', $user->id)
                ->orderBy('rides.departure_time', 'DESC')
                ->get();
        foreach ($rides as &$ride) {
            $ride->departure_station = $this->closestStation($ride->departure_latitude, $ride->departure_longitude);
            $ride->arrival_station = $this->closestStation($ride->arrival_latitude, $ride->arrival_longitude);
        }

        return $rides;
    }
    
    
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ended_rides() {
        $rides = Ride::select(
                        'rides.*', 'bikes.name as bike_name', 'locks.qr_code', 'users.name', 'users.last_name', 'dep_point.latitude as departure_latitude', 'dep_point.longitude as departure_longitude', 'arr_point.latitude as arrival_latitude', 'arr_point.longitude as arrival_longitude'
                )
                ->join('bikes', 'bikes.id', '=', 'rides.bike_id')
                ->join('locks', 'locks.id', '=', 'rides.lock_id')
                ->join('users', 'users.id', '=', 'rides.user_id')
                ->join('points as dep_point', 'dep_point.id', '=', 'rides.departure_id')
                ->join('points as arr_point', 'arr_point.id', '=', 'rides.departure_id')
                ->finalized()
                ->orderBy('rides.departure_time', 'DESC')
                ->get();
        foreach ($rides as &$ride) {
            $ride->departure_station = $this->closestStation($ride->departure_latitude, $ride->departure_longitude);
            $ride->arrival_station = $this->closestStation($ride->arrival_latitude, $ride->arrival_longitude);
        }

        return $rides;
    }

    private function normalDistanceSqrt($lat1, $lon1, $lat2, $lon2) {
        return ($lat2 - $lat1) * ($lat2 - $lat1) + ($lon2 - $lon1) * ($lon2 - $lon1);
    }

    public function closestStation($lat, $lon) {
        $stations = \Potosi\Station::get();
        $closest = $stations[0];
        $minDistance = 999999999;
        foreach ($stations as $st) {
            $distance = $this->normalDistanceSqrt($lat, $lon, $st->latidude, $st->longitude);
            if ($distance < $minDistance) {
                $minDistance = $distance;
                $closest = $st;
            }
        }
        return $closest;
    }

    public function recalculateDistance() {
        $rides = Ride::where('rides.status', 'Finalized')
                ->get();

        foreach ($rides as $ride) {
            $ride->distance = $ride->calculateDistance();
            $ride->save();
        }
        return $rides;
    }

}
