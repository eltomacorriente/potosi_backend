<?php

namespace Potosi\Http\Controllers\Admin;

use Potosi\Ride;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;

class ReportController extends Controller
{
 
    public function ridesByDay() {
        return Ride::selectRaw("count( CAST( departure_time AS DATE) ) as dt_count , CAST( departure_time AS DATE) as dt , SUM(distance) as distance_sum"  )
                ->groupBy( 'dt' )
                ->orderBy( 'dt'  , 'DESC')     
                ->finalized()
                ->get();
    }
}
