<?php

namespace Potosi\Http\Controllers\Admin;

use Potosi\Role;
use Potosi\User;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;

class RoleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Role::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role) {
        $role->name = $request->input("name");
        $role->max_bike_number = $request->input("max_bike_number");
        $role->save();
         return Role::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $role = new Role();
        $role->name = $request->input("name");
        $role->max_bike_number = $request->input("max_bike_number");
        $role->save();
         return Role::all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role) {
        $user = User::where('role_id' , $role->id)->get();
        
        
        if ($role->name != "Administrador" && sizeof($user) == 0) {
            $role->delete();
             return Role::all();
        }else{
            
             return Role::get();
        }
    }

}
