<?php

namespace Potosi\Http\Controllers\Admin;

use Potosi\Bike;
use Potosi\Lock;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;

class BikeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bikes = Bike::get();
        foreach ($bikes as &$bike) {
            $bike->lock;
            if ($bike->lock == null) {
            }else{
                $bike->lock->lastPoint;
            }
        }
        return $bikes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Bike $bike) {
        $locks = Lock::get();
        $bike->lock;
        return view('admin/bike_edit', ['bike' => $bike, 'locks' => $locks]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bike $bike) {
        
        $bike->model = $request->input("model");
        if ($request->input("status") == "enabled") {
            $bike->status = 'enabled';
        } else {
            $bike->status = 'disabled';
        }
        $this->setLock($bike, $request->input("lock"));
        $bike->save();
        return $this->edit($bike);
        // 
        //return $this->index();
    }

    /**
     * function for change the lock in other bike
     *
     * @return \Illuminate\Http\Response
     */
    private function setLock(Bike &$bike, $lock_id) {        
        
        $oldLock = Lock::where('bike_id' , $bike->id)->get();
        foreach($oldLock as $ol) {
            $ol->bike_id = null;
            $ol->save();
        }
        
        $oldBike = Bike::where('lock_id' , $lock_id)->where('id' , '!=' ,$bike->id)->get();
        foreach($oldBike as $ob){
            $ob->lock_id = null;
            $ob->save();
        }
        
        $newLock = Lock::find($lock_id);
        $newLock->bike_id = $bike->id;
        $newLock->save();
        
        $bike->lock_id = $lock_id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $bike = new Bike();
        $bike->name = $request->input("name");
        $bike->model = $request->input("model");
        $bike->status = 'enabled';
        $bike->save();
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Bike  $bike
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bike $bike) {
        $bike->delete();
        return $this->index();
    }

    /**
     * Enable an bike
     *
     * @return \Illuminate\Http\Response
     */
    public function enable(Bike $bike) {
        $bike->status = 'enabled';
        $bike->save();
        return $bike;
    }

    /**
     * Disable an Bike
     *
     * @return \Illuminate\Http\Response
     */
    public function disable(Bike $bike) {
        $bike->status = 'disabled';
        $bike->save();
        return $bike;
    }

}
