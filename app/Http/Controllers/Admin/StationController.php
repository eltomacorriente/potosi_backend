<?php

namespace Potosi\Http\Controllers\Admin;

use Potosi\Station;
use Illuminate\Http\Request;
use Potosi\Http\Controllers\Controller;

class StationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Station::get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Station $station) {
        $station->name = $request->input("name");
        $station->latitude = (float) $request->input("latitude");
        $station->longitude = (float) $request->input("longitude");

        $station->save();
        return Station::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $station = new Station();
        $station->name = $request->input("name");
        $station->latitude = (float)  $request->input("latitude");
        $station->longitude =  (float)  $request->input("longitude");
        $station->save();
        return Station::all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Station  $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station) {
        $station->delete();
        return Station::all();
    }

    /**
     * Enable an station
     *
     * @return \Illuminate\Http\Response
     */
    public function enable(Station $station) {
        $station->status = 'enabled';
        $station->save();
        return $station;
    }

    /**
     * Disable an Station
     *
     * @return \Illuminate\Http\Response
     */
    public function disable(Station $station) {
        $station->status = 'disabled';
        $station->save();
        return $station;
    }

}
