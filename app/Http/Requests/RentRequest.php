<?php

namespace Potosi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Potosi\Ride;
use Potosi\Bike;

class RentRequest extends FormRequest {

    protected $redirect = 'api_error';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        $user = Auth::user();
        return $user->status == 'enabled';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
                //'bike_number' => ['required', 'integer'],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator) {

        $validator->after(function ($validator) {
            if (!$this->bikeExists()) {
                $validator->errors()->add('bike_number', 'La bicicleta no existe');
            } else if (!$this->bikeIsAvailable()) {
                $validator->errors()->add('bike_number', 'La bicicleta no está disponible');
            } else if ($this->bikeIsRented()) {
                $validator->errors()->add('bike_number', 'La bicicleta está a nombre de otra persona');
            } else if ($this->userHasMax()) {
                $validator->errors()->add('bike_number', 'El usuario no puede sacar más bicicletas');
            }
        });
    }
    
    /**
     * Check if bike exits
     *
     * 
     * @return boolean
     */
    private function bikeExists() {
        $this->bike = Bike::where('name', $this->bike_number)->first();
        return ($this->bike != null);
    }

     /**
     * Check if bike is available
     *
     * 
     * @return boolean
     */
    private function bikeIsAvailable() {
        return ($this->bike->status == 'enabled' );
    }
    
      /**
     * Check if bike is already rented
     *
     * 
     * @return boolean
     */
    private function bikeIsRented() {
        $ride = Ride::where('status', "=" , 'In Progress')
                ->where('bike_id', "=" , $this->bike_number)
                ->get();
        return (sizeof($ride) > 0);
    }
    
      /**
     * Check if user can rent
     *
     * 
     * @return boolean
     */
    private function userHasMax() {
        $user = Auth::user();
        $max_bike_number = $user->role->max_bike_number;
        $ride = Ride::where('status', 'In Progress')
                ->where('user_id', $user->id)
                ->get();
        return (sizeof($ride) > $max_bike_number);
    }

}
