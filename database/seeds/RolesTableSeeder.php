<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \Potosi\Role::truncate();
        // Administrador
        \Potosi\Role::create([
            'name' => 'Administrador',
            'max_bike_number' => 2,
        ]);

        // Socio/Canje
        \Potosi\Role::create([
            'name' => 'Canje',
            'max_bike_number' => 3,
        ]);

        // Invitado
        \Potosi\Role::create([
            'name' => 'Invitado',
            'max_bike_number' => 1,
        ]);

        // Colaborador
        \Potosi\Role::create([
            'name' => 'Colaborador de Vivienda',
            'max_bike_number' => 1,
        ]);

        // Independiente
        \Potosi\Role::create([
            'name' => 'Independiente',
            'max_bike_number' => 1,
        ]);
    }

}
