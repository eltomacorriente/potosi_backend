<?php

use Illuminate\Database\Seeder;

class StationsTableSeeder extends Seeder {

    /**
     *
     * @var data with the Station information
     */
    private $dataStations = [
        ["Arcadia", 4.787976, - 73.955884],
        ["Potosí", 4.791256, -73.959021],
        ["Altamira", 4.792216, -73.962986],
        ["Zaragoza", 4.793623, -73.964881],
        ["Victoria", 4.796731, -73.966324],
        ["Granada", 4.799221, -73.968804],
        ["Recodo", 4.797173, -73.960414],
        ["Portería Principal", 4.791210, -73.951879],
        ["Sede Social", 4.793825, -73.956066],
        ["Tennis", 4.794514, -73.956815],
        ["Esquí", 4.794886, -73.963040],
        ["Hípica", 4.798333, -73.963889],
        ["Portería Occidental", 4.796133, -73.971507],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \Potosi\Station::truncate();

        foreach ($this->dataStations as $data) {
            \Potosi\Station::create([
                'name' => $data[0],
                'latitude' => $data[1],
                'longitude' => $data[2],
                'status' => "enabled"
            ]);
        }
    }

}

/*
 * 

 */
