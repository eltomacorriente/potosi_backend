<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        \Potosi\User::truncate();

        \Potosi\User::create([
            'identification_type' => 'CC',
            'identification' => 1020769546,
            'role_id' => 1,
            'name' => 'Camilo',
            'last_name' => 'Camilo',
            'email' => 'caegomezji@gmail.com',
            'gender' => 'Masculino',
            'phone_number' => 3197811730,
            'status' => 'enabled',
            'api_token' => Str::random(60),
            'password' => Hash::make('Camilo12'),
        ]);
    }

}
