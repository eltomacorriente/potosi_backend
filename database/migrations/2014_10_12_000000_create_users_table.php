<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        /*
         * nombres
          apellidos
          num identifacion
          tipo de identificacion
          tipo de usuario ( cange , invitado , colaborador de vivienda , independiente )
          Cange de donde?
          Colaborador de que vivienda -> numero de vivienda
          num telefono
          quitar fecha
         */
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identification_type', 2);
            $table->integer('identification')->unique();
            $table->integer('role_id')->unsigned();
            $table->string('category_information')
                    ->nullable();
            $table->string('name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('gender');
            $table->string('phone_number');
            $table->enum('status', array('enabled', 'disabled'))->nullable();
            $table->string('password');
            $table->string('api_token', 80)
                    ->unique()
                    ->nullable()
                    ->default(null);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
