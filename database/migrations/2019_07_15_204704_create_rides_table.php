<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', array('In Progress', 'Finalized'))->nullable();
            $table->integer('user_id');
            $table->integer('bike_id');
            $table->integer('lock_id');
            $table->integer('time_elapsed')->nullable();
            $table->integer('distance')->nullable();
            $table->integer('departure_id');
            $table->datetime('departure_time');
            $table->integer('arrival_id')->nullable();
            $table->datetime('arrival_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('rides');
    }

}
