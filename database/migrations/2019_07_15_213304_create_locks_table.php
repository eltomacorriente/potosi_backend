<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('locks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei');
            $table->string('mac');
            $table->string('qr_code');
            $table->integer('bike_id')->nullable();  
            $table->enum('status', array('open', 'close'))->nullable();  
            $table->integer('battery_level')->nullable();  
            $table->integer('gsm_signal')->nullable();            
            $table->integer('point_id')->nullable();          
            $table->datetime('date_status')->nullable();  
            $table->datetime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('locks');
    }

}
