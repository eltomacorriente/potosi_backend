<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->double('latitude', 11, 8);
            $table->double('longitude', 11, 8);            
            $table->integer('ride_id')->nullable();
            $table->integer('lock_id')->nullable();
            $table->enum('status', array('open', 'close'))->nullable();
            $table->integer('battery_status')->nullable();
            $table->integer('gsm_signal')->nullable();
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('points');
    }

}
